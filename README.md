Hello, human! Before you begin your exploration, please note the following instructions:

1) Use the file commands.txt, which is in the root folder of the project.

2) Follow the model:

5 5
1 2 N
LMLMLMLMM
3 3 E
MMRMMRMRRM

Where:
[Mesh Size]
[Starting Position]
[Commands]
[Starting Position]
[Commands]

3) Todo list:

* Verify if input data is valid
* Verify if the commands are valid according to the mesh size
* Optimize logic
* Optimize code organization
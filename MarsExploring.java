import java.io.File;
import java.util.*;

public class MarsExploring {

    public static void main(String[] args) {

        try {

            File file = new File("commands.txt");
            Scanner input = new Scanner(file);
            String[] init = new String[3];

            int i = 0;

            while (input.hasNextLine()) {
                String line = input.nextLine();              

                if(i==0){
                    int[] space = Arrays.stream(line.split(" "))
                            .mapToInt(Integer::parseInt)
                            .toArray();
                }
                else if(i % 2 == 1){
                    init = line.split(" ");
                }
                else{
                    char[] commands = line.toCharArray();

                    for (char com : commands) {
                        switch(com){
                            case 'L':
                                switch(init[2]){
                                    case "N":
                                        init[2] = "W";
                                        break;
                                    case "W":
                                        init[2] = "S";
                                        break;
                                    case "S":
                                        init[2] = "E";
                                        break;
                                    case "E":
                                        init[2] = "N";
                                        break;
                                }
                                break;
                            case 'R':
                                switch(init[2]){
                                    case "N":
                                        init[2] = "E";
                                        break;
                                    case "W":
                                        init[2] = "N";
                                        break;
                                    case "S":
                                        init[2] = "W";
                                        break;
                                    case "E":
                                        init[2] = "S";
                                        break;
                                }
                                break;
                            case 'M': 
                                switch(init[2]){
                                    case "N":
                                        init[1] = String.valueOf(Integer.parseInt(init[1]) + 1);
                                        break;
                                    case "W":
                                        init[0] = String.valueOf(Integer.parseInt(init[0]) - 1);
                                        break;
                                    case "S":
                                        init[1] = String.valueOf(Integer.parseInt(init[1]) - 1);
                                        break;
                                    case "E":
                                        init[0] = String.valueOf(Integer.parseInt(init[0]) + 1);
                                        break;
                                }
                                break;
                        }   
                    }

                    System.out.println(String.valueOf(init[0]) + " " + String.valueOf(init[1]) + " " + String.valueOf(init[2]));
                }

                i++;
            }
            input.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}